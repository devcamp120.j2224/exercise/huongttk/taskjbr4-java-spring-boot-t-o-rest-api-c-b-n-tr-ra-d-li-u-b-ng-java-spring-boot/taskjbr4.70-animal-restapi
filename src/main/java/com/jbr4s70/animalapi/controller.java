package com.jbr4s70.animalapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class controller {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList <Cats> newListCats(){
        ArrayList <Cats> listCat = new ArrayList<>();

        Cats cat1 = new Cats("belly");
        Cats cat2 = new Cats("Saly");
        Cats cat3 = new Cats("Lysa");

        cat1.greets();

        listCat.add(cat1);
        listCat.add(cat2);
        listCat.add(cat3);

        return listCat;
    }

    @CrossOrigin
    @GetMapping("/dogs")
    public ArrayList <Dogs> newListDogs(){
        ArrayList <Dogs> listDog = new ArrayList<>();

        Dogs dog1 = new Dogs("Mit");
        Dogs dog2 = new Dogs("Love");
        Dogs dog3 = new Dogs("Cam");

        dog1.greets();
        dog2.greets(dog3);

        listDog.add(dog1);
        listDog.add(dog2);
        listDog.add(dog3);

        return listDog;
    }
}
