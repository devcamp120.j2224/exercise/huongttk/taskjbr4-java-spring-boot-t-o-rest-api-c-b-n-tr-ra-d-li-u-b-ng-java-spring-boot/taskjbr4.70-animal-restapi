package com.jbr4s70.animalapi;

public abstract class Animal {
    private String name = "";

    public Animal(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal [name=" + name + "]";
    }
    
    public String getName() {
        return name;
    }



}
