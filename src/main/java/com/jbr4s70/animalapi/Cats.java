package com.jbr4s70.animalapi;

public class Cats extends Mamal {

    public Cats(String name) {
        super(name);
    }

    public void greets(){
        System.out.println("Cat greeting meow...");
    }

    @Override
    public String toString() {
        return "Cats []";
    }
    
}
