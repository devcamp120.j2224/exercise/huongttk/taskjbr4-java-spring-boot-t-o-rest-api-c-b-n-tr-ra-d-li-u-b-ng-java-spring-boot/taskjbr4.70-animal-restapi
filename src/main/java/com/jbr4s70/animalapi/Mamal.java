package com.jbr4s70.animalapi;

public class Mamal extends Animal {

    public Mamal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Mamal []";
    }
    
    public void greets(){
        System.out.println("Mamal greeting ...");
    }
}
